export const environment = {
  production: false,
  API_ENDPOINT: 'http://localhost:3000/api/v1/'
  // API_ENDPOINT: 'https://api.globalrtis.com/api/v1/'
};
