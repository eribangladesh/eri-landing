import { StartSectionComponent } from './start-section/start-section.component';
import { IntroducingSectionComponent } from './introducing-section/introducing-section.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularFullpageModule } from '@fullpage/angular-fullpage';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { TabsModule } from 'ngx-bootstrap/tabs';
import {
  SwiperConfigInterface,
  SwiperModule,
  SWIPER_CONFIG,
} from 'ngx-swiper-wrapper';
import { FooterSectionComponent } from './ footer-section/footer-section.component';
import { HeaderSectionComponent } from './ header-section/header-section.component';
import { AboutUsSectionComponent } from './about-us-section/about-us-section.component';
import { ChooseThisSectionComponent } from './choose-this-section/choose-this-section.component';
import { ContactUsSectionComponent } from './contact-us-section/contact-us-section.component';
import { LandingPageRoutingModule } from './landing-page-routing.module';
import { LandingPageComponent } from './landing-page.component';
import { PricingSectionComponent } from './pricing-section/pricing-section.component';
import { WorkingProcessSectionComponent } from './working-process-section/working-process-section.component';
import { TryNowSectionComponent } from './try-now-section/try-now-section.component';
import { LottieModule } from 'ngx-lottie';
import { MatVideoModule } from 'mat-video';

import player from 'lottie-web';

export function playerFactory() {
  return player;
}

const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  direction: 'horizontal',
  slidesPerView: 'auto',
};
import { CarouselModule } from 'ngx-owl-carousel-o';

@NgModule({
  declarations: [
    LandingPageComponent,
    HeaderSectionComponent,
    AboutUsSectionComponent,
    ChooseThisSectionComponent,
    WorkingProcessSectionComponent,
    StartSectionComponent,
    PricingSectionComponent,
    ContactUsSectionComponent,
    FooterSectionComponent,
    TryNowSectionComponent,
    IntroducingSectionComponent,
  ],
  imports: [
    LottieModule.forRoot({ player: playerFactory, useCache: true }),
    LandingPageRoutingModule,
    CommonModule,
    AngularFullpageModule,
    TabsModule.forRoot(),
    NgbModule,
    SwiperModule,
    FormsModule,
    ReactiveFormsModule,
    CarouselModule,
    MatVideoModule,
    AccordionModule.forRoot(),
    ModalModule.forRoot(),
  ],
  providers: [
    {
      provide: SWIPER_CONFIG,
      useValue: DEFAULT_SWIPER_CONFIG,
    },
  ],
})
export class LandingPageModule {}
