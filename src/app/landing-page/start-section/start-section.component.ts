import { Component, ViewEncapsulation } from '@angular/core';

export interface SwiperAutoplayInterface {
  disableOnInteraction: false;
}
@Component({
  selector: 'app-start-section',
  templateUrl: './start-section.component.html',
  styleUrls: ['./start-section.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class StartSectionComponent {
  ngclass = 'mat-video-responsive';

  src = 'assets/video/company-introduction.mp4';
  // title = 'NASA Rocket Launch';
  width = 600;
  height = 337.5;
  currentTime = 0;
  autoplay = false;
  preload = true;
  loop = false;
  quality = false;
  download = false;
  fullscreen = true;
  showFrameByFrame = true;
  keyboard = true;
  color = 'primary';
  spinner = 'spin';
  poster = 'assets/video/NASA.jpg';
  overlay = null;
  muted = true;
  constructor() {}
}
