import { Component, TemplateRef, ElementRef, ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-footer-section',
  templateUrl: './footer-section.component.html',
  styleUrls: ['./footer-section.component.scss']
})
export class FooterSectionComponent {
  config: any;
  modalRef: BsModalRef;
  configz = {
    backdrop: true,
    ignoreBackdropClick: false
  };
  constructor(private modalService: BsModalService) {}
  openModalPrivacyModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, this.configz);
  }
  openModalTermsModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, this.configz);
  }
}
