import {
  Component,
  ViewEncapsulation,
  OnInit,
  TemplateRef,
  ViewChild,
  AfterViewInit
} from '@angular/core';
import { NgbTabsetConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-working-process-section',
  templateUrl: './working-process-section.component.html',
  styleUrls: ['./working-process-section.component.scss'],
  providers: [NgbTabsetConfig]
  // encapsulation: ViewEncapsulation.None
})
export class WorkingProcessSectionComponent implements OnInit, AfterViewInit {
  @ViewChild('tab1', { static: true }) tab1: TemplateRef<any>;
  @ViewChild('tab2', { static: true }) tab2: TemplateRef<any>;
  @ViewChild('tab3', { static: true }) tab3: TemplateRef<any>;
  @ViewChild('tab4', { static: true }) tab4: TemplateRef<any>;

  selectedTemplate: any;
  selectedTab: any;

  customClass = 'customClass';

  constructor(config: NgbTabsetConfig) {
    config.justify = 'center';
    config.type = 'pills';
  }

  ngAfterViewInit() {
    this.selectedTemplate = this['tab1'];
    this.selectedTab = 'tab1';
  }

  onSelectTab(tab) {
    this.selectedTemplate = this[tab];
    this.selectedTab = tab;
  }

  openCity(evt, cityName) {
    let i: any, tabcontent: any, tablinks: any;
    tabcontent = document.getElementsByClassName('tabcontent');
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = 'none';
    }
    tablinks = document.getElementsByClassName('tablinks');
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(' active', '');
    }
    document.getElementById(cityName).style.display = 'block';
    evt.currentTarget.className += ' active';
  }

  ngOnInit() {
    document.getElementById('defaultOpen').click();
  }

  callFunc() {
    console.log('i m called');
  }

  // Get the element with id="defaultOpen" and click on it
  // document.getElementById("defaultOpen").click();
}
