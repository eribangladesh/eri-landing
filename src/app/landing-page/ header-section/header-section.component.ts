import { Router, ActivatedRoute } from '@angular/router';
import {
  Component,
  OnInit,
  ViewEncapsulation,
  AfterViewInit
} from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { AnimationItem } from 'lottie-web';
import { AnimationOptions } from 'ngx-lottie';

@Component({
  selector: 'app-header-section',
  templateUrl: './header-section.component.html',
  styleUrls: ['./header-section.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HeaderSectionComponent implements OnInit, AfterViewInit {
  customOptions: OwlOptions = {
    stagePadding: 0,
    loop: true,
    autoplay: true,
    margin: 50,
    autoplayTimeout: 6000,
    autoplaySpeed: 2500,
    navSpeed: 700,
    nav: false,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 1
      },
      1000: {
        items: 1
      }
    }
  };

  options: AnimationOptions = {
    path: '/assets/lottie/scroll-down.json'
  };

  private fragment = 'page2';

  constructor(private router: ActivatedRoute, private _router: Router) {}

  ngOnInit() {
    this.router.fragment.subscribe(fragment => {
      this.fragment = fragment;
    });
  }

  _onClickPage2() {
    console.log('Hello');
    this._router.navigate(['/'], { fragment: 'page2' });
  }

  ngAfterViewInit(): void {
    try {
      document.querySelector('#' + this.fragment).scrollIntoView();
    } catch (e) {}
  }

  scrollToContactTypes() {
    this._router.onSameUrlNavigation = 'reload';
    this._router.navigate(['/'], { fragment: 'page2' });
  }

  animationCreated(animationItem: AnimationItem): void {
    console.log(animationItem);
  }
}
