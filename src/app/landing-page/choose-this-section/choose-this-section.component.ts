import {
  Component,
  OnInit,
  Input,
  HostListener,
  OnChanges
} from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-choose-this-section',
  templateUrl: './choose-this-section.component.html',
  styleUrls: ['./choose-this-section.component.scss']
})
export class ChooseThisSectionComponent implements OnInit, OnChanges {
  all: any;

  @Input()
  cssData: Observable<any>;

  css = '-500px';
  css1 = '-700px';

  vh: any;
  vw: any;

  constructor() {}

  ngOnInit() {
    // this.all = document.getElementsByClassName(
    //   'left-side-item'
    // ) as HTMLCollectionOf<HTMLElement>;

    this.vh = window.innerHeight * 0.01;
    this.vw = window.innerWidth * 0.01;

    // Then we set the value in the --vh custom property to the root of the document
    document.documentElement.style.setProperty('--vh', `${this.vh}px`);

    console.log('hhh', this.vh + ' ' + this.vw);
  }

  ngOnChanges() {
    console.log('jjjj', this.cssData);
    if (this.cssData) {
      this.css = '0';
      this.css1 = '0';
    }
  }
}
