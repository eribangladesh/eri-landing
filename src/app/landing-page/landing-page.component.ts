import {
  Component,
  ElementRef,
  OnInit,
  ViewChild,
  HostListener,
  ViewEncapsulation
} from '@angular/core';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit {
  @ViewChild('fullpageRef', { static: false })
  fp_directive: ElementRef;
  config: any;
  fullpage_api: any;
  // tslint:disable-next-line:max-line-length
  isShow: boolean;
  topPosToStartShowing = 100;
  header: any;
  icon: any;
  activePanel: any = 'active';
  currentPage: any;
  animateIcon: any;
  all: HTMLCollectionOf<HTMLElement>;
  flag: boolean;
  activeMobileMenu: any = 'sec1';
  constructor() {
    console.log();
    this.config = {
      // fullpage options
      // licenseKey: 'YOUR LICENSE KEY HERE',
      licenseKey: 'Cqd081D$l6',
      anchors: [
        'page1',
        'page2',
        'page3',
        'page4',
        'page5',
        'page6',
        'page7',
        'page8',
        'page9',
        'page10'
      ],
      slidesNavigation: true,
      slidesNavPosition: 'bottom',
      menu: '#menu',
      scrollingSpeed: 900,
      scrollOverflow: true,
      fitToSection: true,
      dragAndMove: 'mouseonly',
      // fullpage callbacks
      onLeave: (origin, destination) => {
        // console.log('origin:', origin.index, 'destination:', destination.index);
        this.currentPage = destination.index;
        if (destination.index === 1) {
          this.flag = true;
          console.log(this.flag);
        } else {
          this.flag = false;
          console.log(this.flag);
        }
        // console.log(origin);
        // console.log(destination);
        // console.log(direction);
        const elementz = document.getElementById('menuz');
        const element = document.getElementById('menu');
        if (this.currentPage !== 0) {
          elementz.classList.remove('transparent');
          element.classList.remove('transparent-menu');
          elementz.classList.add('block');
          element.classList.add('block-menu');
        } else {
          element.classList.remove('block-menu');
          elementz.classList.remove('block');
          element.classList.add('transparent-menu');
          elementz.classList.add('transparent');
        }
      },
      afterResize: () => {
        console.log('After resize');
      }
      // afterLoad: destination => {
      //   if (destination.index == 1) {
      //     console.log('asdsa');
      //   }
      // }
      // afterLoad: (origin, destination, direction) => {
      //   this.currentPage = destination.index;
      //   // console.log(origin);
      //   // console.log(destination);
      //   // console.log(direction);
      //   const element = document.getElementById('menu');
      //   if (this.currentPage !== 0) {
      //     element.classList.remove('transparent-menu');
      //     element.classList.add('block-menu');
      //   } else {
      //     element.classList.remove('block-menu');
      //     element.classList.add('transparent-menu');
      //   }
      // }
    };
  }
  getRef(fullPageRef) {
    this.fullpage_api = fullPageRef;
    if (window.innerWidth <= 1024) {
      this.fullpage_api.destroy('all');
    }
  }

  ngOnInit() {
    if (this.flag) {
      document.getElementById('left-side-item').style.left = '0';
    }

    this.header = document.querySelector('.header');
    this.icon = document.querySelector('.icon-container');
    this.animateIcon = document.querySelector('.bars');
  }

  toggleMenu() {
    this.header.classList.toggle('menu-open');
    this.animateIcon.classList.toggle('active');
  }

  clickMenuItem(data) {
    document.getElementById(data).scrollIntoView();
    this.header.classList.toggle('menu-open');
    this.animateIcon.classList.toggle('active');
    this.activeMobileMenu = data;
  }

  @HostListener('window:scroll', ['$event'])
  onScroll(event) {
    // console.log('window scroll', event);

    const sec1 = this.isScrolledIntoView(document.getElementById('sec1'));
    const sec2 = this.isScrolledIntoView(document.getElementById('sec2'));
    const sec3 = this.isScrolledIntoView(document.getElementById('sec3'));
    const sec4 = this.isScrolledIntoView(document.getElementById('sec4'));
    const sec5 = this.isScrolledIntoView(document.getElementById('sec5'));
    // const sec6 = this.isScrolledIntoView(document.getElementById('sec6'));
    const sec7 = this.isScrolledIntoView(document.getElementById('sec7'));
    const sec9 = this.isScrolledIntoView(document.getElementById('sec9'));

    if (sec1) {
      this.activeMobileMenu = 'sec1';
    } else if (sec3) {
      this.activeMobileMenu = 'sec3';
    } else if (sec4) {
      this.activeMobileMenu = 'sec4';
    } else if (sec5) {
      this.activeMobileMenu = 'sec5';
    } else if (sec7) {
      this.activeMobileMenu = 'sec7';
    } else if (sec9) {
      this.activeMobileMenu = 'sec9';
    }

    // console.log(sec1);
  }

  isScrolledIntoView(el) {
    let rect = el.getBoundingClientRect();
    let elemTop = rect.top;
    let elemBottom = rect.bottom;

    // Only completely visible elements return true:
    let isVisible = elemTop >= 0 && elemBottom <= window.innerHeight;
    // Partially visible elements return true:
    isVisible = elemTop < window.innerHeight && elemBottom >= 0;
    return isVisible;
  }
}

// onclick="this.classList.toggle('active')"
