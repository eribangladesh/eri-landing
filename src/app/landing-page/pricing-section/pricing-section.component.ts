import { Component, ViewEncapsulation } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-pricing-section',
  templateUrl: './pricing-section.component.html',
  styleUrls: ['./pricing-section.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PricingSectionComponent {
  customOptionss: OwlOptions = {
    stagePadding: 40,
    loop: false,
    margin: 25,
    nav: false,
    center: true,
    dots: false,
    mouseDrag: true,
    touchDrag: true,
    startPosition: 1,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 3
      },
      1000: {
        items: 5
      }
    }
    // stagePadding: 50,
    // margin: 10
  };
  customOptionz: OwlOptions = {
    stagePadding: 0,
    loop: false,
    margin: 20,
    nav: false,
    dots: false,
    mouseDrag: false,
    touchDrag: false,
    autoWidth: true
    // center: true,

    // stagePadding: 50,
    // margin: 10
  };
  constructor() {}
}
