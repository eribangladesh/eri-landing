import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import Swal from "sweetalert2";
import { AuthService } from "../../services/auth.service";

@Component({
  selector: "app-contact-us-section",
  templateUrl: "./contact-us-section.component.html",
  styleUrls: ["./contact-us-section.component.scss"]
})
export class ContactUsSectionComponent implements OnInit {
  mailForm: FormGroup;

  orgTypeData: any[] = [
    { id: 0, name: "Factory" },
    { id: 1, name: "Brand" },
    { id: 2, name: "Group" },
    { id: 3, name: "Project" },
    { id: 4, name: "Consultant" }
  ];

  purposeData: any[] = [
    { id: 0, name: "Product Information" },
    { id: 1, name: "Pricing" },
    { id: 2, name: "Support" },
    { id: 3, name: "Other" }
  ];

  constructor(private fb: FormBuilder, private mailService: AuthService) {
    this.mailForm = this.fb.group({
      org_name: [null, [Validators.required]],
      org_type: [null, [Validators.required]],
      name: [null, [Validators.required]],
      email: [null, [Validators.required, Validators.email]],
      cell_no: [null, [Validators.required]],
      purpose: [null, [Validators.required]],
      message: [null, [Validators.required]]
    });
  }

  ngOnInit() {}

  getFormControl(name) {
    return this.mailForm.controls[name];
  }

  sendMail() {
    this.mailService.mail(this.mailForm.value).subscribe(
      (result: any) => {
        Swal.fire({
          title: "Thanks!",
          text: "Our support team will contact with you soon",
          icon: "success",
          confirmButtonText: "Close"
        });
      },
      error => {
        Swal.fire({
          title: "Error!",
          text: "Email has not sent! please try again",
          icon: "error",
          confirmButtonText: "Close"
        });
      }
    );
  }
}
