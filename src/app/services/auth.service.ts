import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient, private router: Router) {}

  EndPoint = environment.API_ENDPOINT + `auth/`;

  mail(payload: any) {
    return this.http.post(this.EndPoint + 'mailFromLanding', payload);
  }
}
