import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [AppComponent],
  imports: [
    HttpClientModule,
    BrowserAnimationsModule,
    BrowserModule.withServerTransition({ appId: 'my-app' }),
    RouterModule.forRoot(
      [
        {
          path: '',
          loadChildren: () =>
            import('./landing-page/landing-page.module').then(
              m => m.LandingPageModule
            ),
          pathMatch: 'full'
        },
        {
          path: '**',
          redirectTo: '',
          pathMatch: 'full'
        }
      ],
      {
        anchorScrolling: 'enabled',
        useHash: false
      }
    ),
    TransferHttpCacheModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
